package main

import "fmt"

func main() {
	//contoh 1
	var names [3]string

	names[0] = "irawan"
	names[1] = "si"
	names[2] = "kasep"

	fmt.Println(names[0])
	fmt.Println(names[1])
	fmt.Println(names[2])

	//contoh 2
	var numbers = [4]int{
		5,
		4,
		7,
		8,
	}

	fmt.Println(numbers)

	//cek panjang array , gunakan function len()
	fmt.Println(len(numbers))
}
