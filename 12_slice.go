package main

import "fmt"

func main() {
	var months = [...]string{
		"Januari",
		"Februari",
		"Maret",
		"April",
		"Mei",
		"Juni",
		"Juli",
		"Agustus",
		"September",
		"Oktober",
		"November",
		"Desember",
	}

	var slice1 = months[4:7]
	fmt.Println(slice1)
	fmt.Println(len(slice1)) //cek panjang slice
	fmt.Println(cap(slice1)) //cek kapasitas sisa 

	

	slice2 := months[10:]
	fmt.Println(slice2)

	//contoh append array atau menambahkan isi array, jika kapasitas sudah penuh, maka akan membuat array baru
	slice3 := append(slice2, "Hello World")
	fmt.Println(slice3)
	slice3[1] = "Edit Desember"
	fmt.Println(slice3)
	fmt.Println(slice2)
	fmt.Println(months)

	//contoh membuat slice dari awal
	//dengan function make(param1, param2, param3)
	//param1 tipe slice
	//param2 length array
	//param3 capcity array
	newSlice := make([]string, 2, 5)
	newSlice[0] = "Irawan"
	newSlice[1] = "sikasep"

	fmt.Println(newSlice)
	fmt.Println(len(newSlice))
	fmt.Println(cap(newSlice))

	//contoh copy slice
	//dengan function copy
	fromSlice := months[:] //variable yang akan di copy
	toSlice := make([]string, len(fromSlice), cap(fromSlice)) //variable baru penampung hasil copy

	copy(toSlice, fromSlice)
	toSlice[2] ="maret wkwk"
	
	fmt.Println(toSlice)
	fmt.Println(fromSlice)

	//perbedaan slice dan array

	iniArray := [5]int{1,2,3,4,5}
	iniSlice := []int{1,2,3,4,5}

	fmt.Println(iniArray)
	fmt.Println(iniSlice)
}