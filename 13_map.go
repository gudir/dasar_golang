package main

import "fmt"

func main() {
	// catatan
	//function map ini mirip dengan tipe data object kalo di javascript

	//versi deklarasi panjangnya
	// var person map[string]string = map[string]string{
	// 	"name":    "Irawan",
	// 	"address": "cimahi",
	// }

	//versi singkatnya
	person := map[string]string {
		"name":    "Irawan",
		"address": "cimahi",
	}

	//jika ingin tambah isi di map nya
	person["title"] = "Programmer"
	
	//cek output
	fmt.Println(person)
	fmt.Println(person["name"])
	fmt.Println(person["address"])
	fmt.Println(person["title"])

	//cara membuat map dari awal data kosong

	var book map[string]string = make(map[string]string)
	book["title"] = "Belajar GOlang dalam 1 menit"
	book["author"] = "Irawan"
	book["isbn"] = "000000";
	
	//cek output
	fmt.Println(book);
	//cek berapa jumlah isi map dengan function len
	fmt.Println(len(book))

	//cara hapus isi map
	delete(book, "isbn")
	fmt.Println(book)
}