package main

import "fmt"

func main() {
	name := "Kasep"

	if name == "Irawanss" {
		fmt.Println("IF IS TRUE")
	} else if name == "Kasep" {
		fmt.Println("ELSE IF CONDITION")
	} else {
		fmt.Println("CONDITION IS FALSE, masuk ke ELSE")
	}
}