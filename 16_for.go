package main

import "fmt"

func main() {
	counter := 1
	//contoh 1
	for counter <= 10 {
		fmt.Println("Perulangan ke", counter)
		counter++
	}

	//contoh2
	for i:=1; i<=10; i++ {
		fmt.Println("Perulangan ke", i);
	}

	//contoh 3
	book := []string{"Komik", "Novel", "Buku Pemrograman"}

	for z:=0; z<len(book); z++ {
		fmt.Println(book[z]);
	}

}