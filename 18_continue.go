package main

import "fmt"

func main() {
	for i := 0; i <= 10; i++ {
		if i%2 == 0 {
			//perulangan stop di sini tetapi perulangan selanjutnya tetap di proses
			continue
		}
		fmt.Println("Perulangan Ke", i)
	}
}