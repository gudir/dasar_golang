package main

import "fmt"

func sayHello() {
	fmt.Println("Hello from function")
}

func main() {
	sayHello()
}