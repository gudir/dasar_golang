package main

import "fmt"

func getValue() (string, string) {
	return "value1", "value2"
}

func main() {
	val1, val2 := getValue()
	fmt.Println(val1, val2)
}
