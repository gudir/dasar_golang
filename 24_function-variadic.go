package main

import "fmt"

func sumAll(numbers ...int) int {
	total := 0

	for _, value := range numbers {
		total += value
	}

	return total
}

func main() {
	jumlah := sumAll(10, 10, 10, 20)
	fmt.Println(jumlah)

	//jika tipe datanya slice/array
	angka := []int{10, 20, 30}
	jumlah2 := sumAll(angka...)
	fmt.Println(jumlah2)
}
