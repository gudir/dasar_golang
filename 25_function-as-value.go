package main

import "fmt"

func sayHello(name string) string {
	return "Hello " + name
}

func main() {
	hello := sayHello
	result := hello("Brother")

	fmt.Println(result)
	fmt.Println(hello("Wakwaw"))
}
