package main

import "fmt"

//agar tidak panjang di parameter nya bisa dengan menggunakan interface
type Wakanda func() string

func sayHelloWithFilter(name string, filter func(string) string, wakanda Wakanda) string {
	nameFiltered := filter(name)
	return "Hello " + nameFiltered + " " + wakanda()
}

func spamFilter(name string) string {
	if name == "Anjing" {
		return "..."
	} else {
		return name
	}
}

func sayWakanda() string {
	return "Wakanda"
}

func main() {
	//langsung kirim sebagai parameter function
	fmt.Println(sayHelloWithFilter("Anjing", spamFilter, sayWakanda))

	//atau bisa function nya di jadikan variable dulu
	filterWord := spamFilter
	fmt.Println(sayHelloWithFilter("Hello", filterWord, sayWakanda))
}
