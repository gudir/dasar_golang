package main

import "fmt"

//function factorial menggunakan looping
func factorialLoop(value int) int {
	result := 1
	for i := value; i > 0; i-- {
		result *= i //result = result * i
	}
	return result
}

func factorialRecursive(value int) int {
	if value == 1 {
		return 1
	} else {
		return value * factorialRecursive(value-1)
	}
}

func main() {
	loop := factorialLoop(5)
	fmt.Println(loop)
	//manualnya
	fmt.Println(5 * 4 * 3 * 2 * 1)

	//menggunakan recursive function
	fmt.Println(factorialRecursive(5))
}
