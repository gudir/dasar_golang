package main

import "fmt"

func main() {
	name := "test"
	counter := 0

	increment := func() {
		name = "coba"
		counter++
		fmt.Println(name)
	}
	//variable name akan ngambil dari yang di luar function
	fmt.Println(name)
	increment()
}
