package main

import "fmt"

func logging() {
	fmt.Println("Memanggil function logging")
}

func jalankanAplikasi() {
	//function logging akan di jalankan ketika function jalankanAplikasi, atau di panggil paling terakhir
	defer logging()
	fmt.Println("Menjalankan Application")
}

func main() {
	jalankanAplikasi()
}
