package main

import "fmt"

func akhirAplikasi() {
	fmt.Println("Aplikasi Selesai")
}

func runApplication(isError bool) {
	defer akhirAplikasi()
	if isError {
		//function runApplication akan berakhir disini dan aplikasi golangnya akan exit jika isError true
		//untuk function defer akhirAplikasi akan tetap di jalankan
		panic("APLIKASI ERROR")
	}
	fmt.Println("Aplikasi di jalankan")
}

func main() {
	runApplication(true)

	//print ini tidak akan kepanggil jika panic , karena app golang nya exit
	fmt.Println("Hello")
}
