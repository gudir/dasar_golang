// fungsi recover() adalah fungsi untuk menangkap pesan dari panic
// kalo di laravel atau js mah si try catch
package main

import "fmt"

func akhirAplikasi() {
	message := recover()
	if message != nil {
		fmt.Println(message)
	}

	fmt.Println("Aplikasi Selesai")
}

func runApplication(isError bool) {
	defer akhirAplikasi()
	if isError {
		//function runApplication akan berakhir disini dan aplikasi golangnya akan exit jika isError true
		//untuk function defer akhirAplikasi akan tetap di jalankan
		panic("APLIKASI ERROR")
	}
	//recover tidak akan menangkap pesan dari panic jika penempatan recover nya di bawah function panic
	//karena aplikasi akan berhenti di function panic jika ada error,
	//karena itu kita bisa memanfaatkan defer
	//message := recover()

	fmt.Println("Aplikasi di jalankan")
}

func main() {
	runApplication(true)
}
