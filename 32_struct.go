package main

import "fmt"

type Customer struct {
	Name, Address string
	Age           int
}

func main() {
	var cust Customer
	cust.Name = "John Doe"
	cust.Address = "Planet"
	cust.Age = 14

	fmt.Println(cust)

	//atau jika ingin di buat variable
	cust2 := Customer {
		Name: "Asep",
		Address: "Cibodas",
		Age: 16,
	}

	fmt.Println(cust2)
}