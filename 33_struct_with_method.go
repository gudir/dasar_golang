package main

import "fmt"

type User struct {
	Name, Address string
	Age           int
}

//cara 1
func sayHelloUser(user User, name string) {
	fmt.Println("Hello", name, "My name is", user.Name)
}

//cara 2 , struct Method
func (user User) sayHelloUser2(name string) {
	fmt.Println("Hello", name, "My name is", user.Name)
}

func main() {
	var user User
	user.Name = "John Doe"
	user.Address = "Planet"
	user.Age = 14

	//cara1
	sayHelloUser(user, "Ujang")

	//cara2 , jadi seolah olah struct mempunyai sebuah function, user. adalah parameternya
	user.sayHelloUser2("Ujang")
}