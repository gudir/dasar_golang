package main

import "fmt"

type HasName interface {
	GetName() string
}

func sayHai(hasName HasName) {
	fmt.Println("Hello", hasName.GetName())
}

//Contoh 1
type Person struct {
	Name string
}

//GetName contract dari interface
func (person Person) GetName() string {
	return person.Name
}
//END CONTOH 1

//Contoh 2
type Animal struct {
	NameAnimal string
}

func (animal Animal) GetName() string {
	return animal.NameAnimal
}

func main() {
	var eko Person
	eko.Name ="PZN"
	sayHai(eko)

	var binatang Animal
	binatang.NameAnimal = "Kucing"
	sayHai(binatang)
}