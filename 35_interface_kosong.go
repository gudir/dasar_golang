package main

import "fmt"

//return type interface kosong bisa return tipe data apapun
func Ups() interface {} {
	//controh kita return integer
	//return 1
	//contoh kita return string
	return "Wkwkwk"
}

func main() {
	var data interface {} = Ups()
	fmt.Println(data)
}