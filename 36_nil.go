package main

import "fmt"

func bikinMap(name string) map[string]string{
	if name == "" {
		return nil
	}else{
		return map[string]string{
			"name": name,
		}
	}
}

func main() {
	//contoh 1
	var person map[string]string = nil
	
	if person["name"] == "" {
		fmt.Println("Data kosong")
	}else{
		fmt.Println(person)
	}
	
	//contoh 2
	var person2 map[string]string = bikinMap("")

	if person2 == nil {
		fmt.Println("Data kosong")
	}else{
		fmt.Println(person2)
	}


}