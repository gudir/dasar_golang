package main

import (
	"errors"
	"fmt"
)
 
func pembagi(nilai int, nilaiBagi int) (int, error) {
	if nilaiBagi==0 {
		return 0, errors.New("Pembagi tidak boleh 0")
	}else{
		result := nilai/nilaiBagi
		return result, nil
	}
}

func main() {
	//contoh 1
	var contohError = errors.New("Ups Error")
	fmt.Println(contohError)

	//contoh2
	hasil, err := pembagi(100, 0)
	if err == nil {
		fmt.Println("Hasil pembagi adalah", hasil)
	} else {
		fmt.Println("Error", err.Error())
	}
}