package main

import "fmt"

func random() interface{} {
	return "sebuah kata"
}

func main() {
	var result interface{} = random()

	switch value := result.(type) {
	case string:
		fmt.Println("Value", value, "Is String")
	case int:
		fmt.Println("Value", value, "Is Integer")
	default:
		fmt.Println("Is Unknown")
	}
}