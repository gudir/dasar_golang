package main

import "fmt"

type Address struct {
	City, Province, Country string
}

func main() {
	var address1 Address = Address{
		City:     "Cimahi",
		Province: "Jawa Barat",
		Country:  "Indonesia",
	}

	address2 := address1
	address2.City = "Bandung"

	// address 1 tidak berubah menjadi bandung, value nya masih Cimahi, 
	//karena di golang itu saat assign variable nya bukan by referensi tetapi di duplikasi/copy
	fmt.Println("Address1",address1)
	//sehingga di address 2 value City nya Bandung, dan di address1 masih cimahi,
	//jadi address1 nilai nya tidak sama dengan address2
	//karena address2 menduplikasi address1 bukan mereferensikan address1
	fmt.Println("Address2", address2)

	//jika ingin mereferensikan,
	//bisa menambahkan operator "&" saat assign variable nya, contoh:
	address3 := &address1
	address3.City = "Cihampelas"
	//address1 berubah menjadi Cihampelas mengikuti address3 , karena sekarang saat assign variable, assignnya by referensi
	fmt.Println("Address1", address1)
	fmt.Println("Address3",address3)


	//tapi bagaimana jika struct nya di definisikan ulang ? contohnya
	// address4 := address1
	// address4 = Address {
	// 	City: "Malang",
	// 	Province: "Jawa Timur",
	// 	Country: "Indonesia",
	// }
	//jika struct nya di definisikan ulang, address1 value nya tidak berubah, tidak mengikuti hasil address4
	//karena struct nya di definisikan ulang
	//fmt.Println("Address1", address1)
	//fmt.Println("Address4",address4)
	//jika ingin address1 nya mengikuti address4,
	//maka saat assign variablenya tambahkan tanda "*" didepan variablenya, contoh:
	address4 := &address1
	*address4 = Address {
		City: "Malang",
		Province: "Jawa Timur",
		Country: "Indonesia",
	}
	fmt.Println("Address1", address1)
	fmt.Println("Address4",address4)
}