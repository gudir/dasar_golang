package main

import "fmt"

type Address struct {
	City, Province, Country string
}

//nilai country tidak berubah
func changeAddressToIndonesiaSalah(address Address) {
	address.Country = "Indonesia"
}

//Jika ingin nilai country berubah makan Address nya harus pointer
//dengan menambahkan tandak "*"
func changeAddressToIndonesiaBenar(address *Address) {
	address.Country = "Indonesia"
}

func main() {
	//contoh salah
	address1 := Address{"Cimahi", "Jawa Barat", "Korea"}
	changeAddressToIndonesiaSalah(address1)

	fmt.Println(address1) //Country tidak berubah jadi Indonesia

	//contoh benar
	address2 := Address{"Cimahi", "Jawa Barat", "Korea"}
	changeAddressToIndonesiaBenar(&address2)
	fmt.Println(address2)
}