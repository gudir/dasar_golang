package main

import "fmt"

type Man struct {
	Name string
}

//jika tidak pakai pointer, hasil nya tidak akan berubah ke struct , 
//karena jika tidak pakai pointer, itu akan menduplikasi buka mereferensi
func (man Man) cobasalah() {
	man.Name = "Mr " + man.Name
}

//contoh pakai pointer
func (man *Man) cobabenar() {
	man.Name = "Mr " + man.Name
}

func main() {
	var nama Man = Man{"Naruto"}
	nama.cobasalah()

	fmt.Println("Hello ", nama.Name) //Name masih naruto bukan Mr Naruto

	nama.cobabenar();
	fmt.Println("Hello ", nama.Name)
}