package main

import "fmt"

func main() {
	var name string

	name = "Irawan"
	fmt.Println(name);
	

	//bisa juga tanpa kata kunci var, bisa dengan kata kunci :=
	address := "World"
	fmt.Println(address)

	age := 28
	fmt.Println(age)

	//bisa juga di multiple sekaligus
	var (
		firstName = "irawan"
		lastName = "sikasep"
	)

	fmt.Println(firstName)
	fmt.Println(lastName)
}