package main

import "fmt"

func main() {
	const firstName = "irawan"
	const lastName = "sikasep"

	//error
	//firstName = "asd" <=== tidak bisa assign karen constant
	fmt.Println(firstName)

	//atau bisa juga multiple cara mendefiniskannya
	const (
		address = "World"
		age = 28
	)
}