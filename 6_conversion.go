package main

import "fmt"

func main() {
	//nilai awal integer32
	var nilai32 int32 = 100000
	//di konversi jadi integer64
	var nilai64 int64 = int64(nilai32)
	//di konversi lagi jadi intger8
	//hati hati ketika konversi , contoh jika di konversi dari int32 ke int8
	//akan ada perubahan nilai di karenakan int8 tidak dapat menampung nilai dari int32
	var nilai8 int8 = int8(nilai32)

	fmt.Println(nilai32)
	fmt.Println(nilai64)
	fmt.Println(nilai8)

	//contoh 2
	var name = "irawan"
	//tipe data byte
	var e byte = name[0]
	//di konversi kembali jadi string
	var eString = string(e)

	fmt.Println(name)
	fmt.Println(e)
	fmt.Println(eString)
}