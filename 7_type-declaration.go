package main

import "fmt"

func main() {
	//inisialisasi type data declaration
	//contoh bikin tipe-data string dengan nama variable
	type NoKTP int
	type Married bool

	//variable noKtpUser tipe datanya integer karena sudah di inisialisasi oleh type decalaration NoKTP
	var noKtpUser NoKTP = 12345678
	//variable marriedStatus tipe datanya boolean karena sudah di inisialisasi oleh type decalaration Married
	var marriedStatus = true

	fmt.Println(noKtpUser)
	fmt.Println(marriedStatus)
}