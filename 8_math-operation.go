package main

import "fmt"

func main() {
	//contoh 1
	var a = 10
	var b = 11
	var c = a + b

	fmt.Println(c)

	//contoh2
	var result = 10 + 50
	fmt.Println(result)

	//contoh 3
	var i = 10
	i += 15 // i = i + 15
	fmt.Println(i)
}
