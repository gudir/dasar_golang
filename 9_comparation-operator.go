package main

import "fmt"

func main() {
	var a = "irawan"
	var b = "irawan"

	var result bool = a == b
	fmt.Println(result)

	var value1 = 100
	var value2 = 200

	fmt.Println(value1 < value2)
	fmt.Println(value1 > value2)
}
